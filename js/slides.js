$(function(){

	$('.mosaico .container .mosaico-wraper').slick({
		centerMode: true,
		slidesToShow:5,
		arrows:false,
		responsive:[

			{
				breakpoint:768,
				settings:{
					arrows:false,
					centerMode:false,
					slidesToShow:3
				}
			},

			{
				breakpoint:580,
				settings:{
					arrows:false,
					centerMode:false,
					slidesToShow:2
				}
			},

			{
				breakpoint:380,
				settings:{
					arrows:false,
					centerMode:true,
					slidesToShow:1
				}
			}

		]
	});

	$('.tratamentos .container').slick({
		centerMode:false,
		slidesToShow:3,
		arrows:false,
		infinite:false,
		responsive:[

			{
				breakpoint:768,
				settings:{
					arrows:false,
					dots:true,
					infinite:false,
					centerMode:false,
					slidesToShow:2
				}
			},

			{
				breakpoint:480,
				settings:{
					arrows:false,
					dots:true,
					infinite:false,
					centerMode:false,
					slidesToShow:1
				}
			}

		]
	});


	$('.depoimentos .container').slick({
		centerMode:false,
		slidesToShow:1,
		arrows:false,
		infinite:true,
		dots:true,
		autoplay:true,
		autoplaySpeed:2000,
		speed:500 
	});

});